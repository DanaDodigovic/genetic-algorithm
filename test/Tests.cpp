#include <catch2/catch.hpp>
#include <iostream>

#include "GeneticAlgorithm.hpp"
#include "Chromosome.hpp"
#include "Population.hpp"
#include "Selection.hpp"
#include "Crossover.hpp"
#include "Mutation.hpp"
//#include "Function.hpp"

std::ostream& operator<<(std::ostream& os, const std::vector<bool>& v)
{
    for (const auto& el : v) { os << el << " "; }
    return os;
}

std::ostream& operator<<(std::ostream& os,
                         const std::vector<std::vector<bool>>& v)
{
    for (const auto& el : v) { os << el << " "; }
    return os;
}

std::ostream& operator<<(std::ostream& os, const std::vector<double>& v)
{
    for (const auto& el : v) { os << el << " "; }
    return os;
}

TEST_CASE("randomize")
{
    Bounds b1({-5, 5});
    Bounds b2({-1, 2});
    Bounds b3({-10, 2});
    //FloatChromosome fc({b1, b2, b3});
    F3 fitness_function;
    BinaryChromosome bc({b1, b2, b3}, 2);
    Population p(10, bc, fitness_function);
    std::cout << p << std::endl;
    //std::array<Chromosome*, 3> a =  p.sample_of_3();
    //for (unsigned i = 0; i < a.size(); i++) {
        //std::cout << *a[i] << std::endl; 
    //}
}
