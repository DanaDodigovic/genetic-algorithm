#!/bin/sh
EXE=./LAB4
FILE=res/results/out3.txt
rm -f ${FILE}
POP_SIZES=(30 50 100 200)
MUT_PROBS=(0.1 0.3 0.6 0.9)
for POP_SIZE in "${POP_SIZES[@]}"
do
    for MUT_PROB in "${MUT_PROBS[@]}"
    do
        printf '%3d, %.1f: ' $POP_SIZE $MUT_PROB >> ${FILE}
        for i in {1..30}
        do
            $EXE --chromosome-type float \
                 --bounds -50,150 \
                 --dimensionality 2 \
                 --fitness-func F6 \
                 --n-iter 150000 \
                 --pop-size $POP_SIZE \
                 --mutation gauss \
                 --mut-prob $MUT_PROB \
                 --crossover arithmetic | \
                      awk '{print $NF}' | \
                      sed -z 's/\n/ /g' >> ${FILE}
        done
        echo >> ${FILE}
    done
done
