import os


def FindGCCDirectory():
    """ Finds GCC version based on the "/usr/include/c++" directory contents.
        Only one subdirectory is expected which is assumed to have the name of
        GCC version. That directory contains C/C++ header files. """
    gcc_version = os.listdir('/usr/include/c++')[0]
    return os.path.join('/usr/include/c++', gcc_version)


def FlagsForFile(filename):
    """ Returns compilation flags for filename passed in. YouCompleteMe calls
        this method for each opened file to get compilation flags. """
    flags = [
        '-Wall',
        '-Wextra',
        '-pedantic-errors',
        '-Iinclude',
        '-Ilib/include',
        '-isystem' + FindGCCDirectory(),
        '-isystem/usr/local/include',
        '-isystem/usr/include/'
    ]

    filetype = filename.split(".")[-1]
    if filetype in ('c', 'h'):
        flags += ['-xc', '-std=c17']
    elif filetype in ('cpp', 'hpp'):
        flags += ['-xc++', '-std=c++17']

    return {
        'flags':    flags,
        'do_cache': True
    }
