#ifndef CROSSOVER_HPP
#define CROSSOVER_HPP

#include "Chromosome.hpp"

class Crossover {
  public:
    Crossover() {}
    void mate(Chromosome& parent1, Chromosome& parent2, Chromosome& child);

  private:
    virtual void
    mate_(Chromosome& parent1, Chromosome& parent2, Chromosome& child) = 0;
};

class SinglePointCrossover : public Crossover {
  private:
    void
    mate_(Chromosome& parent1, Chromosome& parent2, Chromosome& child) override;
};

class UniformCrossover : public Crossover {
  private:
    void
    mate_(Chromosome& parent1, Chromosome& parent2, Chromosome& child) override;
};

class AverageCrossover : public Crossover {
  private:
    void
    mate_(Chromosome& parent1, Chromosome& parent2, Chromosome& child) override;
};

class ArithmeticCrossover : public Crossover {
  private:
    void
    mate_(Chromosome& parent1, Chromosome& parent2, Chromosome& child) override;
};

#endif

