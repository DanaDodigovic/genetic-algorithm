#ifndef MUTATION_HPP
#define MUTATION_HPP

#include "Chromosome.hpp"

class Mutation {
  protected:
    double probability = 0.3;

  public:
    Mutation(double probability);
    void mutate(Chromosome& c);

  private:
    virtual void mutate_(Chromosome& c) = 0;
};

class SimpleBinaryMutation : public Mutation {
  public:
    SimpleBinaryMutation(double probability);

  private:
    void mutate_(Chromosome& c) override;
};

class GaussianMutation : public Mutation {
  public:
    GaussianMutation(double probability);

  private:
    void mutate_(Chromosome& c) override;
};

#endif
