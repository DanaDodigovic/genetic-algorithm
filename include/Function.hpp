#ifndef FUNCTION_HPP
#define FUNCTION_HPP

#include <filesystem>

class Chromosome;

class FitnessFunction {
  private:
    unsigned call_counter = 0;
    bool minimization     = true;

  public:
    FitnessFunction(bool minimization = true);
    unsigned get_n_of_calls() const;
    double operator()(const Chromosome& c) const;

  private:
    virtual double evaluate(const Chromosome& c) const = 0;
};

class F1 : public FitnessFunction {
  public:
    F1(bool minimization = true);

  private:
    double evaluate(const Chromosome& c) const override;
};

class F3 : public FitnessFunction {
  public:
    F3(bool minimization = true);

  private:
    double evaluate(const Chromosome& c) const override;
};

class F6 : public FitnessFunction {
  public:
    F6(bool minimization = true);

  private:
    double evaluate(const Chromosome& c) const override;
};

class F7 : public FitnessFunction {
  public:
    F7(bool minimization = true);

  private:
    double evaluate(const Chromosome& c) const override;
};

class F : public FitnessFunction {
  private:
    std::filesystem::path path;

  public:
    F(bool minimization = true, std::filesystem::path path = "");

  private:
    double evaluate(const Chromosome& c) const override;
};

#endif
