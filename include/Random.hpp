#ifndef RANDOM_HPP
#define RANDOM_HPP

#include <random>

class Random {
  private:
    static std::random_device random_generator;
    static std::mt19937 pseudo_random_generator;

  public:
    static std::mt19937& generator();
};

#endif
