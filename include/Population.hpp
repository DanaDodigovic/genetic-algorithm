#ifndef POPULATION_HPP
#define POPULATION_HPP

#include "Function.hpp"
#include "Chromosome.hpp"
#include "DereferenceIterator.hpp"

#include <vector>
#include <memory>

class Population {
  private:
    using RefIter = ReferenceIterator<
        std::vector<std::unique_ptr<Chromosome>>::const_iterator>;
    Chromosome& best;

    std::vector<std::unique_ptr<Chromosome>> chromosomes;

  public:
    Population(std::size_t pop_size,
               Chromosome& c,
               FitnessFunction& fitness_function,
               bool empty = false);

    Population(const Population& other);
    Population& operator=(const Population& other);

    Population(Population&& other) noexcept;
    Population& operator=(Population&& other) noexcept;

    ~Population() = default;

    Chromosome& get_best() const;
    bool best_has_changed();
    void add_chromosome(Chromosome& chromosome);
    std::unique_ptr<Chromosome> get_chromosome(std::size_t index) const;

    RefIter begin() const;
    RefIter end() const;

    std::size_t size() const;
    std::array<Chromosome*, 3> sample() const;

    friend std::ostream& operator<<(std::ostream& os, const Population& p);
};

#endif
