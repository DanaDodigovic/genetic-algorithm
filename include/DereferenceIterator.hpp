#ifndef DEREFERENCE_ITERATOR_HPP
#define DEREFERENCE_ITERATOR_HPP

#include <cstddef>

/** Reference iterator over a collection that stores smart pointers. This means
 * that iteration is performed over references to objects. */
template<class BaseIterator>
class ReferenceIterator : public BaseIterator {
  public:
    using value_type = typename BaseIterator::value_type::element_type;
    using pointer    = value_type*;
    using reference  = value_type&;

    ReferenceIterator(const BaseIterator& other) : BaseIterator(other) {}

    reference operator*() const { return *(this->BaseIterator::operator*()); }

    reference operator[](std::size_t n) const
    {
        return *(this->BaseIterator::operator[](n));
    }
};

/** Pointer iterator over a collection that stores smart pointers. This means
 * that iteration is performed over raw pointers to objects. */
template<class BaseIterator>
class PointerIterator : public BaseIterator {
  public:
    using value_type = typename BaseIterator::value_type::element_type;
    using pointer    = value_type*;
    using reference  = value_type&;

    PointerIterator(const BaseIterator& other) : BaseIterator(other) {}

    pointer operator*() const { return this->BaseIterator::operator*().get(); }

    reference operator[](std::size_t n) const
    {
        return *(this->BaseIterator::operator[](n));
    }
};

#endif
