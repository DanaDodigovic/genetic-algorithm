#include "GeneticAlgorithm.hpp"
#include "Configuration.hpp"

int main(int argc, char* argv[])
{
    cxxopts::Options options("LAB4", "Simple GA");

    options.add_options()(
        "c,chromosome-type", "Chromosome type", cxxopts::value<std::string>())(
        "b,bounds", "Bounds", cxxopts::value<std::string>())(
        "d,dimensionality", "Number of variables", cxxopts::value<unsigned>())(
        "f,fitness-func", "Fitness function", cxxopts::value<std::string>())(
        "i,n-iter", "Number of iterations", cxxopts::value<unsigned>())(
        "s,pop-size", "Population size", cxxopts::value<std::size_t>())(
        "m,mutation", "Mutation type", cxxopts::value<std::string>())(
        "p,mut-prob", "Mutation probability", cxxopts::value<double>())(
        "x,crossover", "Crossover type", cxxopts::value<std::string>())(
        "r,precision", "Binary precision", cxxopts::value<unsigned>())(
        "a,filepath", "File path", cxxopts::value<std::string>());

    cxxopts::ParseResult parse_result = options.parse(argc, argv);
    Configuration config(parse_result);

    GenerationRouletteWheelGA ga(*config.crossover,
                                 *config.mutation,
                                 config.pop_size,
                                 config.n_of_iters);

    std::cout << *ga.optimize(*config.fitness_function,
                              *config.initial_chromosome)
              << std::endl;
}
