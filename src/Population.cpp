#include "Population.hpp"

#include <algorithm>
#include <iostream>

#include "Random.hpp"

/* Creates population of pop_size random chromosomes or empty population. */
Population::Population(std::size_t pop_size,
                       Chromosome& c,
                       FitnessFunction& fitness_function,
                       bool empty)
    : best(c)
{
    best.set_fitness(fitness_function(best));

    if (!empty) {
        chromosomes.resize(pop_size);
        for (auto& chromosome : chromosomes) {
            std::unique_ptr<Chromosome> c_copy = c.clone();
            c_copy->randomize();
            chromosome = std::move(c_copy);
            chromosome->set_fitness(fitness_function(*chromosome));
        }
    }
}

Population::Population(const Population& other) : best(other.best)
{
    chromosomes.reserve(other.chromosomes.size());
    for (auto& c : other) { chromosomes.push_back(c.clone()); }
}

Population& Population::operator=(const Population& other)
{
    chromosomes.clear();
    chromosomes.reserve(other.chromosomes.size());
    for (auto& c : other) { chromosomes.push_back(c.clone()); }
    best = get_best();
    return *this;
}

Population::Population(Population&& other) noexcept
    : best(other.best), chromosomes(std::move(other.chromosomes))
{}

Population& Population::operator=(Population&& other) noexcept
{
    best        = other.best;
    chromosomes = std::move(other.chromosomes);
    return *this;
}

Chromosome& Population::get_best() const
{
    return *std::max_element(
        begin(), end(), [](Chromosome& c1, Chromosome& c2) {
            return c1.get_fitness() < c2.get_fitness();
        });
}

bool Population::best_has_changed()
{
    if (std::abs(best.get_fitness() - get_best().get_fitness()) < 1e-12) {
        return false;
    }

    best = get_best();
    return true;
}

void Population::add_chromosome(Chromosome& chromosome)
{
    chromosomes.push_back(chromosome.clone());
}

Population::RefIter Population::begin() const
{
    return RefIter(chromosomes.begin());
}

Population::RefIter Population::end() const
{
    return RefIter(chromosomes.end());
}

std::size_t Population::size() const
{
    return chromosomes.size();
}

std::unique_ptr<Chromosome> Population::get_chromosome(std::size_t index) const
{
    return chromosomes[index]->clone();
}

std::array<Chromosome*, 3> Population::sample() const
{
    using ConstPtrIter = PointerIterator<
        std::vector<std::unique_ptr<Chromosome>>::const_iterator>;
    ConstPtrIter begin_iter(chromosomes.begin());
    ConstPtrIter end_iter(chromosomes.end());

    std::array<Chromosome*, 3> samp;
    std::sample(begin_iter, end_iter, samp.begin(), 3, Random::generator());
    return samp;
}

std::ostream& operator<<(std::ostream& os, const Population& p)
{
    for (const auto& c : p) { os << c << '\n'; }
    return os;
}
