#include "Crossover.hpp"
#include "Random.hpp"

#include <algorithm>

void Crossover::mate(Chromosome& parent1,
                     Chromosome& parent2,
                     Chromosome& child)
{
    mate_(parent1, parent2, child);
}

void SinglePointCrossover::mate_(Chromosome& parent1,
                                 Chromosome& parent2,
                                 Chromosome& child)
{
    BinaryChromosome& b_parent1 = dynamic_cast<BinaryChromosome&>(parent1);
    BinaryChromosome& b_parent2 = dynamic_cast<BinaryChromosome&>(parent2);
    BinaryChromosome& b_child   = dynamic_cast<BinaryChromosome&>(child);

    for (std::size_t i = 0; i < b_child.size(); i++) {
        std::uniform_int_distribution<int> dist(0, b_child.at(i).size());
        int crossover_index = dist(Random::generator());

        std::copy(b_parent1[i].begin(),
                  b_parent1[i].begin() + crossover_index,
                  b_child[i].begin());

        std::copy(b_parent2[i].begin() + crossover_index,
                  b_parent2[i].end(),
                  b_child[i].begin() + crossover_index);
    }

    b_child.set_phenotype_from_genotype();
}

void UniformCrossover::mate_(Chromosome& parent1,
                             Chromosome& parent2,
                             Chromosome& child)
{
    BinaryChromosome& a       = dynamic_cast<BinaryChromosome&>(parent1);
    BinaryChromosome& b       = dynamic_cast<BinaryChromosome&>(parent2);
    BinaryChromosome& b_child = dynamic_cast<BinaryChromosome&>(child);

    BinaryChromosome r(a.get_bounds(), b.get_precision());
    for (std::size_t i = 0; i < b_child.size(); i++) {
        for (std::size_t j = 0; j < b_child[i].size(); j++) {
            b_child.at(i, j) = (a.at(i, j) & b.at(i, j)) |
                               (r.at(i, j) & (a.at(i, j) | b.at(i, j)));
        }
    }

    b_child.set_phenotype_from_genotype();
}

void AverageCrossover::mate_(Chromosome& parent1,
                             Chromosome& parent2,
                             Chromosome& child)
{
    for (std::size_t i = 0; i < child.size(); i++) {
        child[i] = 0.5 * (parent1[i] + parent2[i]);
    }
}

void ArithmeticCrossover::mate_(Chromosome& parent1,
                                Chromosome& parent2,
                                Chromosome& child)
{
    static std::uniform_real_distribution<double> dist(0, 1);
    double a = dist(Random::generator());
    for (std::size_t i = 0; i < child.size(); i++) {
        child[i] = a * parent1[i] + (1 - a) * parent2[i];
    }
}
