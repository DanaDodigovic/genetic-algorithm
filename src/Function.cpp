#include "Function.hpp"
#include "Chromosome.hpp"

#include <fstream>
#include <cmath>

FitnessFunction::FitnessFunction(bool minimization) : minimization(minimization)
{}

double FitnessFunction::operator()(const Chromosome& c) const
{
    return minimization ? -evaluate(c) : evaluate(c);
}

F1::F1(bool minimization) : FitnessFunction(minimization) {}

double F1::evaluate(const Chromosome& c) const
{
    return 100 * std::pow(c[1] - std::pow(c[0], 2), 2) + std::pow(1 - c[0], 2);
}

F3::F3(bool minimization) : FitnessFunction(minimization) {}

double F3::evaluate(const Chromosome& c) const
{
    unsigned i   = 1;
    double value = 0;
    for (auto& el : c) { value += std::pow(el - i++, 2); }
    return value;
}

F6::F6(bool minimization) : FitnessFunction(minimization) {}

double F6::evaluate(const Chromosome& c) const
{
    double ss = 0;
    for (auto& el : c) { ss += std::pow(el, 2); }
    return 0.5 + (std::pow(std::sin(std::sqrt(ss)), 2) - 0.5) /
                     std::pow(1 + 0.001 * ss, 2);
}

F7::F7(bool minimization) : FitnessFunction(minimization) {}

double F7::evaluate(const Chromosome& c) const
{
    double ss = 0;
    for (auto& el : c) { ss += std::pow(el, 2); }
    return std::pow(ss, 0.25) *
           (1 + std::pow(std::sin(50 * std::pow(ss, 0.1)), 2));
}

F::F(bool minimization, std::filesystem::path path)
    : FitnessFunction(minimization), path(path)
{}

double F::evaluate(const Chromosome& c) const
{
    std::ifstream in(path);

    double x, y, f;
    constexpr double e = 2.71828;
    double sum         = 0;
    double n           = 0;
    while (in >> x >> y >> f) {
        double f_calc = std::sin(c[0] + c[1] * x) +
                        c[2] * std::cos(x * (c[3] + y)) *
                            (1 / (1 + std::pow(e, std::pow(x - c[4], 2))));
        sum += std::pow(f_calc - f, 2);
        ++n;
    }

    return sum / n;
}
