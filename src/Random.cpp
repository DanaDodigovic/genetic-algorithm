#include "Random.hpp"

std::random_device Random::random_generator;
std::mt19937 Random::pseudo_random_generator(random_generator());

std::mt19937& Random::generator()
{
    return pseudo_random_generator;
}
