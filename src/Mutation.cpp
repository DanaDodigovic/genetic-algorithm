#include "Mutation.hpp"
#include "Random.hpp"

#include <iostream>

Mutation::Mutation(double probability) : probability(probability) {}

void Mutation::mutate(Chromosome& c)
{
    mutate_(c);
}

SimpleBinaryMutation::SimpleBinaryMutation(double probability)
    : Mutation(probability)
{}

// Mutates one bit randomly.
void SimpleBinaryMutation::mutate_(Chromosome& c)
{
    BinaryChromosome& bc = dynamic_cast<BinaryChromosome&>(c);

    bc.flip_random_bits();
    bc.set_phenotype_from_genotype();
}

GaussianMutation::GaussianMutation(double probability) : Mutation(probability)
{}

// Mutates one bit randomly.
void GaussianMutation::mutate_(Chromosome& c)
{
    std::normal_distribution<double> dist(0, 5);
    std::bernoulli_distribution coin(probability);

    double rnd;
    for (unsigned i = 0; i < c.size(); i++) {
        if (coin(Random::generator())) {
            do {
                rnd = dist(Random::generator());
            } while (c[i] + rnd < c.get_bounds()[i].lower ||
                     c[i] + rnd > c.get_bounds()[i].upper);
            c[i] += rnd;
        }
    }
}
