#include "GeneticAlgorithm.hpp"
#include "Random.hpp"

#include <random>
#include <algorithm>
#include <iostream>

GeneticAlgorithm::GeneticAlgorithm(Crossover& crossover,
                                   Mutation& mutation,
                                   std::size_t pop_size,
                                   unsigned n_iters)
    : crossover(crossover),
      mutation(mutation),
      pop_size(pop_size),
      n_iters(n_iters)
{}

Elimination3TournamentGA::Elimination3TournamentGA(Crossover& crossover,
                                                   Mutation& mutation,
                                                   std::size_t pop_size,
                                                   unsigned n_iters)
    : GeneticAlgorithm(crossover, mutation, pop_size, n_iters)
{}

std::unique_ptr<Chromosome>
Elimination3TournamentGA::optimize(FitnessFunction& fitness_function,
                                   Chromosome& c)
{
    Population population(
        pop_size, c, fitness_function); // Creates random population.

    best = &population.get_best();

    for (std::size_t iter = 0; iter < n_iters; ++iter) {
        auto random_chromosomes = get_sorted_chromosomes(population);

        Chromosome& worst = *random_chromosomes[0];
        Chromosome& p1    = *random_chromosomes[1];
        Chromosome& p2    = *random_chromosomes[2];
        Chromosome& child(worst);

        crossover.mate(p1, p2, child);
        mutation.mutate(child);

        child.set_fitness(fitness_function(child));

        if (population.best_has_changed()) {
            std::cerr << std::setw(7) << std::left << iter
                      << population.get_best() << std::endl;
        }
    }

    return population.get_best().clone();
}

std::array<Chromosome*, 3>
Elimination3TournamentGA::get_sorted_chromosomes(const Population& population)
{
    auto random_chromosomes = population.sample();
    std::sort(random_chromosomes.begin(),
              random_chromosomes.end(),
              [](Chromosome* c1, Chromosome* c2) {
                  return c1->get_fitness() < c2->get_fitness();
              });
    return random_chromosomes;
}

GenerationRouletteWheelGA::GenerationRouletteWheelGA(Crossover& crossover,
                                                     Mutation& mutation,
                                                     std::size_t pop_size,
                                                     unsigned n_iters)
    : GeneticAlgorithm(crossover, mutation, pop_size, n_iters)
{}

std::unique_ptr<Chromosome>
GenerationRouletteWheelGA::optimize(FitnessFunction& fitness_function,
                                    Chromosome& c)
{
    Population population(
        pop_size, c, fitness_function); // Creates random population.

    for (std::size_t iter = 0; iter < n_iters; ++iter) {
        D = calculate_pop_goodness(population);
        Population new_population(pop_size, c, fitness_function, true);
        new_population.add_chromosome(population.get_best()); // Elitism.

        for (std::size_t i = 0; i < pop_size - 1; ++i) {
            auto p1 = get_random_chromosome(population);
            auto p2 = get_random_chromosome(population);
            Chromosome& child(*p1);

            crossover.mate(*p1, *p2, child);
            mutation.mutate(child);

            child.set_fitness(fitness_function(child));
            new_population.add_chromosome(child);
        }

        if (population.get_best().get_fitness() !=
            new_population.get_best().get_fitness()) {
            std::cerr << std::setw(7) << std::left << iter
                      << new_population.get_best() << std::endl;
        }

        population = new_population;
    }

    return population.get_best().clone();
}

std::unique_ptr<Chromosome> GenerationRouletteWheelGA::get_random_chromosome(
    const Population& population) const
{
    std::uniform_real_distribution<double> dist(0, D);
    double rnd = dist(Random::generator());

    double sum   = 0;
    double index = 0;
    for (const auto& c : population) {
        sum += -1 / c.get_fitness();
        if (rnd <= sum) { break; }
        index++;
    }

    return population.get_chromosome(index);
}

double
GenerationRouletteWheelGA::calculate_pop_goodness(const Population& population)
{
    double goodness = 0;
    for (const auto& c : population) { goodness += -1 / c.get_fitness(); }
    return goodness;
}
